package com.pruebaFranco.practica.services;

import com.pruebaFranco.practica.model.Alumno;
import com.pruebaFranco.practica.repository.AlumnoRepository;

import java.util.List;

public interface AlumnoService {

    Alumno create(Alumno request);

    Alumno searchIdAlumno(int id);

    List<Alumno>listar();

    Integer countAlumno();

    String listAlumnoString();
}
