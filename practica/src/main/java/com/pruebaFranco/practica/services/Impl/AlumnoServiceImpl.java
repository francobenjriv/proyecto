package com.pruebaFranco.practica.services.Impl;

import com.pruebaFranco.practica.model.Alumno;
import com.pruebaFranco.practica.repository.AlumnoRepository;
import com.pruebaFranco.practica.services.AlumnoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AlumnoServiceImpl implements AlumnoService {


    @Autowired
    AlumnoRepository alumnoRepository;

    @Override
    public Alumno create(Alumno request) {
        Alumno alumno = alumnoRepository.save(request);
        return alumno;


    }


    public Alumno searchIdAlumno(int id) {

        return alumnoRepository.searchIdAlumno(id);

    }

    public List<Alumno> listar() {

        return alumnoRepository.findAll();
    }

    @Override
    public Integer countAlumno() {

        Integer countAlumno = alumnoRepository.countAlumno();

        Integer respuesta;


        if (countAlumno < 6) {

            respuesta = 500;
        } else {

            respuesta = 1000;

        }
        return respuesta;
    }

    @Override
    public String listAlumnoString() {

        List<Alumno> listAlumno =  alumnoRepository.findAll();


        String mensaje="";

        for (Alumno al:listAlumno) {

            mensaje = mensaje + "\n" + al.getNombre();

        }


        return mensaje;
    }

}
