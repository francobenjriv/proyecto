package com.pruebaFranco.practica.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
@AllArgsConstructor
@NoArgsConstructor
@Setter @Getter
@Entity
@Table(name = "info_Alumno")
public class Alumno {
    @Id
    @Column (name = "id_Alumno")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idAlumno ;

    @Column(name = "nom_Alumno")
    private String nombre ;

    @Column(name= "apell_Alumno")
    private String apellidos ;

    @Column(name = "ed_Alumno")
    private Integer edad ;




}
