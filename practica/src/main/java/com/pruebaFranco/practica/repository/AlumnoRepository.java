package com.pruebaFranco.practica.repository;

import com.pruebaFranco.practica.model.Alumno;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AlumnoRepository extends JpaRepository<Alumno, Integer> {

    @Query(value = "SELECT * FROM info_alumno WHERE id_Alumno =:id ", nativeQuery = true)
    Alumno searchIdAlumno(@Param("id") int id);


    @Query(value = "select count(*) from info_alumno",nativeQuery = true)
    Integer countAlumno();





}
