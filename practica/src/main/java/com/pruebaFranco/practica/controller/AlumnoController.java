package com.pruebaFranco.practica.controller;


import com.pruebaFranco.practica.model.Alumno;
import com.pruebaFranco.practica.services.AlumnoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/alumno")
public class AlumnoController {

    @Autowired
    AlumnoService alumnoService;

    @PostMapping("/crear")
    ResponseEntity<Alumno> createUser(@RequestBody Alumno request) {
        Alumno alumno = alumnoService.create(request);
        return new ResponseEntity<>(alumno, HttpStatus.OK);

    }

    @GetMapping("/search/{id}")
    ResponseEntity<Alumno> searchIdAlumno(@PathVariable int id) {

        Alumno alumno = alumnoService.searchIdAlumno(id);
        return new ResponseEntity<>(alumno, HttpStatus.OK);

    }
    @GetMapping("/list")
    ResponseEntity<List<Alumno>> listAlumn(){

        List<Alumno> alumno =alumnoService.listar();
        return new ResponseEntity<List<Alumno>>(alumno,HttpStatus.OK);

    }

    @GetMapping("/count")
    ResponseEntity<Integer>countAlumno(){
        return new ResponseEntity<Integer>(alumnoService.countAlumno() , HttpStatus.OK);
    }

    @GetMapping("/listString")
    ResponseEntity<String> listAlumnoString(){

        String listAlumnoString =alumnoService.listAlumnoString();
        return new ResponseEntity<String>(listAlumnoString,HttpStatus.OK);

    }




    //se conecta con la interfaz de services y luego se conecta con la clase de servicesy
    //de ahi se vuelve a conectar mediante autowired con la interfaz repository y este se va
    //a conectar con la base de datos mediante Jpa


}
